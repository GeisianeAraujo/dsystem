import { Component } from '@angular/core';

@Component({
  selector: 'dsystem-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent {
  title = 'dsystem';
  public text = "Meu botão";
}
