import { ButtonComponent } from './button.component';

export default {
  title: 'Button',
  component: ButtonComponent,
};

export const ToStorybook = () => ({
  component: ButtonComponent,
  props: {
    text: "meu texto aqui"
  },
});
