import { Component } from '@angular/core';

@Component({
  selector: 'dsystem-list',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent {
  title = 'dsystem';
  public text = "Minha lista";
}
