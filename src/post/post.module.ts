import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { PostComponent } from './post.component';

@NgModule({
  declarations: [
    PostComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [PostComponent]
})
export class AppModule { }
