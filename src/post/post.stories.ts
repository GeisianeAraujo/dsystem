import { PostComponent } from './post.component';

export default {
  title: 'Post',
  component: PostComponent,
};

export const ToStorybook = () => ({
  component: PostComponent,
  props: {
  },
});
