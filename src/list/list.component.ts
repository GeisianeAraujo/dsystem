import { Component } from '@angular/core';

@Component({
  selector: 'dsystem-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  title = 'dsystem';
  public text = "Minha lista";
}
