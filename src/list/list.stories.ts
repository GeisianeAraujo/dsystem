import { ListComponent } from './list.component';

export default {
  title: 'List',
  component: ListComponent,
};

export const ToStorybook = () => ({
  component: ListComponent,
  props: {
    text: "meu texto aqui"
  },
});
