# DesignSystem

## Links úteis:
- [Documentação do npmjs - creating and publishing scoped public packages](https://docs.npmjs.com/creating-and-publishing-scoped-public-packages)
- 

## Após criar o repositorio

1 - Instale o lerna globalmente com: `npm i lerna -g`

Para testar, verifiquei se o comando `lerna --help` tem resultado.

2 - Crie o arquivo `lerna.json` no repositório principal com o seguinte conteudo:

```json
{
  "version": "independent",
  "npmClient": "npm",
  "command": {
    "publish": {
      "ignoreChanges": [
        "ignored-file",
        "*.md"
      ],
      "message": "chore(release): publish"
    },
    "bootstrap": {
      "ignore": "component-*",
      "npmClientArgs": [
        "--no-package-lock"
      ]
    }
  },
  "packages": [
    "src/*"
  ]
}
```


## Estrutura de pastas para pacotes:
A estrutura base deve ser `/src/nome-do-pacote`, e dentro dessa pasta deve conter o `package.json`

Dessa maneira:
```
design-system/src/nome-do-pacote
  package.json
```

o conteudo do `package.json` segue o seguinte padrão, onde:
`@nome-da-org` é o nome da organização, por exemplo:
```json
{
  "name": "@nome-da-org/nome-do-pacote",
  "version": "0.0.1",
  "private": false
}
```

## Criando pacotes
Crie sempre a estrutura básicao que o próprio lerna gera utilizando o comando 

`lerna create nome-do-pacote`

Agora o lerna criou pra você uma estrutura básica em `./src/nome-do-pacote`.

Altere o `package.json` para adicionar o nome da `organização`, por exemplo:

Se seu pacote se chama `button`, e sua organização se chama `@dsystem`, altere de:

```json
  {
    "name": "button"
  }
```

para:

```json
  {
    "name": "@dsystem/button"
  }
```

E remova as seguintes configurações do `package.json`:

```json
  {
    "main": "lib/button.js",
    "directories": {
      "lib": "lib",
      "test": "__tests__"
    },
    "files": [
      "lib"
    ]
  }
```


## Para publicar pacotes
Crie seu pacote, conforme as instruções acima, e para publicar siga os passos:

Observações:
- As alterações que deseja publicar, devem estar comitadas e não pode ter arquivos alterados e não comitados no projeto.

1 - Rode o comando `lerna publish` (não utilize o NPM PUBLISH).

Se você receber o erro: `lerna ERR! E402 You must sign up for private packages` é porque você está tentando publicar um pacote público, para isso é necessário adicionar ao `package.json` do `pacote` a seguinte configuração:

```json
  "publishConfig": {
    "access": "public"
  }
```

Agora você consegue publicar esse pacote como público utilizando novamente o comando `lerna publish`.

2 - Ele vai abrir um menu para selecionar a nova versão do pacote, e isso para todos os pacotes alterados que precisam ser publicados. 

Por exemplo: 

`Select a new version for @dsystem/button (currently 0.0.1) (Use arrow keys)`

Leia mais em: [semver.org - versionamento semantico](https://semver.org/lang/pt-BR/)

3 - Selecione uma versão para atualizar o pacote especificado

4 - Ele pergunta se você quer publicar os pacotes, e lhe mostra o nome dos pacotes a serem publicados, digite `y` para publicar.

5 - Se a resposta for: `lerna success published 1 packages` e o valor de pacotes for maior que 0, você obteve sucesso em publicar o pacote.

6 - Para instalar o pacote em outro projeto ficaria:
`npm i @nome-da-org/nome-do-pacote`

## Erros comuns

- Make sure you're authenticated properly ¯\_(ツ)_/¯

Erro erro é devido a falta de autenticação, para corrigir:

- Utilize `npm whoami` para verificar se está logado, caso obtenha um erro, utilize o comando abaixo

-  `npm adduser` e faça login com sua conta do `npmjs`

